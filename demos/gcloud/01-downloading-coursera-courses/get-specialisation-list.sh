#!/bin/bash

start=0
step=300
while [ $start -lt 999 ]
do
  echo $start

  file=$(printf "specialization-list-%04d.json" $start)
  echo "File: $file"
  
  if [ ! -f $file ]; then
    curl -o $file "https://api.coursera.org/api/onDemandSpecializations.v1?start=$start&limit=$step"
  fi

  start=$[$start + $step]
done

(
  echo "Slug,Name"

  for file in specialization-list*.json
  do
    length=$(cat $file | jq ".elements | length")
    index=0
    while [ $index -lt $length ]
    do
      slug=$(cat $file | jq ".elements[$index] | .slug" | tr -d '"' )
      name=$(cat $file | jq ".elements[$index] | .name" | tr -d '"' )
      echo "$slug,\"$name\""
      index=$[$index + 1]
    done
  done
) | tee list-of-specialisations.txt
