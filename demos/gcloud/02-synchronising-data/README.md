# This shows how to share data between a VM in GCP and your laptop or the EBI farm

## Getting started
Start by copying the scripts from **../00-managing-projects** here, configure the **env.sh** if you want to customise it, and create a project and a virtual machine with the **create-project.sh** and **create-vm.sh** scripts.

SSH into the machine and install the **rsync** package:
```
./ssh.sh
sudo yum install -y rsync
```

Next, create some dummy data files using the **make-data.sh** script. This creates a **data** directory with subdirectory structure and some random files in it.

## Sync the files to your VM
Run the **sync-to-vm.sh** script. This uses **rsync**, telling it to use the **ssh.sh** script to manage the connection, which it then uses to push the data to the VM.

SSH into the VM and check the data is there:
```
./ssh.sh
find data -type f -ls
 33703960      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:26 data/0/a/5/file.dat
 50332458      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:26 data/0/a/6/file.dat
 83886218      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:26 data/0/b/5/file.dat
100663432      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:26 data/0/b/6/file.dat
...
```

Create some more files, for syncing back to your laptop:

```
for d in data/*/*/*; do cp $d/file.dat $d/file.2.dat ; done
find data -type f -ls
 33703960      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:26 data/0/a/5/file.dat
 33703967      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:50 data/0/a/5/file.2.dat
 50332458      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:26 data/0/a/6/file.dat
 50332533      4 -rw-r--r--   1  wildish  wildish        81 Oct 14 16:50 data/0/a/6/file.2.dat
...
```

## Syncing the files back to your laptop
Now exit from your VM and run the **sync-from-vm.sh** script. It's identical to the **sync-to-vm.sh** script, apart from the direction of transfer. You should see that it only fetches the new files.

## Syncing to/from the EBI cluster
The same procedure will work on the EBI cluster. The only wrinkles are that you need a modern version of Python 3, since the EBI cluster is running a pre-historic version of Linux, and you have to install **gcloud** there yourself and re-create the configuration.

To install Python 3 with **miniconda**, do this:
```
curl -o miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
sh miniconda.sh -b
source miniconda3/bin/activate
```

You will need to add that **source miniconda3/bin/activate** line to your login scripts, probably **~/.bash_profile**

To install **gcloud**, check the Google documentation.

To re-create the configuration, copy these scripts to a directory on the EBI nodes, including the **.projectname** file, then run these commands:
```
. ./env.sh
gcloud config configurations create $project
gcloud config set core/project $project
gcloud config set compute/region $region
gcloud config set compute/zone $zone
gcloud config set core/account ${USER}@ebi.ac.uk
```

Alternatively, you can copy the contents of your **~/.config/gcloud/configurations/*** directory from your laptop to EBI, and all your configurations will be synchronised correctly.

Check this works by running the **ssh.sh** script, you should get logged into your VM. If so, then the **sync-from-vm.sh** script will also work from EBI. You can now use these scripts to syncronise a directory between your laptop, your VM, and the EBI login nodes!

**N.B.**: Note that to move files between EBI and GCP you must run your commands on the EBI cluster, not on the Google VM. That's because the EBI cluster is behind a firewall, so is not visible to Google. That's nothing to do with Google, and there's no easy way I know of to tunnel through the firewall. In any case, it doesn't matter, since you can run on the cluster and move data in either direction.