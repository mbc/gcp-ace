#!/bin/bash

dest="$HOME/slurm/test"
if [ ! -d $dest ]; then
  mkdir -p $dest
fi

(
  echo "#!/bin/bash"
  echo "#SBATCH --output $dest/test-slurm-batch-%J.out"
  echo "#SBATCH --error  $dest/test-slurm-batch-%J.err"
  echo "#SBATCH --nodes=1"
  echo "#SBATCH --exclusive"
  echo " "
  echo "hostname"
  echo "sleep 60"
) | tee test-slurm-batch.sh

for i in `seq 0 5`
do
  for partition in $(sinfo --noheader --format "%R")
  do
    sbatch --partition $partition < test-slurm-batch.sh
  done
done

sleep 2
squeue

echo " "
echo "Partition info:"
sinfo --format "%P %z %m"